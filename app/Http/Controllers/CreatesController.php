<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Menu;
use App\Employee;

class CreatesController extends Controller
{
    public function home(){
    	$articles = Article::all();
    	return view('home', ['articles' => $articles]);
    }

    public function servicemanagement(){
        $articles = Article::all();
        return view('servicemanagement', ['articles' => $articles]);
    }

    public function menumanagement(){
    	$menus = Menu::all();
    	return view('menumanagement', ['menus' => $menus]);
	}

    public function employeemanagement(){
        $employees = Employee::all();
        return view('employeemanagement', ['employees' => $employees]);  //gi retrieve tanang data gikan db mao ni ang mubasa gikan db padung sa table nga gibuhat
    }

    public function add(Request $request){
    	$this->validate($request, [
    			'title' => 'required',
    			'description' => 'required'	
    		]);
    		$articles = new Article;
    		$articles->title = $request->input('title');
    		$articles->description = $request->input('description');
    		$articles->save();
    		return redirect('/servicemanagement')->with('info', 'Service Added Successfully!');

    }

    public function update($id){
    	$articles = Article::find($id);
    	return view('update', ['articles' => $articles]);
    }

    public function edit(Request $request, $id){
    	$this->validate($request, [
    			'title' => 'required',
    			'description' => 'required'	
    		]);
    		$data = array(
    			'title' => $request->input('title'),
    			'description' => $request->input('description')
    		);
    		Article::where('id', $id)
    		->update($data);
    		return redirect('/servicemanagement')->with('info', 'Service Updated Successfully!');

    }

    public function delete($id){
    	Article::where('id', $id)
    	->delete();
    	return redirect('/servicemanagement')->with('info', 'Service Deleted Successfully!');
    }

    public function addmenu(Request $request){
        $this->validate($request, [
                'food' => 'required',
                'price' => 'required'   
            ]);
            $menus = new Menu;
            $menus->food = $request->input('food');
            $menus->price = $request->input('price');
            $menus->save();
            return redirect('/menumanagement')->with('info', 'New Menu Added Successfully!');

    }
    
    public function updatemenu($id){
        $menus = Menu::find($id);
        return view('updatemenu', ['menus' => $menus]);
    }


    public function editmenu(Request $request, $id){
        $this->validate($request, [
                'food' => 'required',
                'price' => 'required' 
            ]);
            $data = array(
                'food' => $request->input('food'),
                'price' => $request->input('price')
            );
            Menu::where('id', $id)
            ->update($data);
            return redirect('/menumanagement')->with('info', 'Menu Successfully Updated!');

    }

    public function deletemenu($id){
        Menu::where('id', $id)
        ->delete();
        return redirect('/menumanagement')->with('info', 'Menu Successfully Deleted!');
    }

    public function addemployee(Request $request){
        $this->validate($request, [
                'firstname' => 'required',
                'familyname' => 'required',
                'contactnumber' => 'required',
                'position' => 'required'  
            ]);
            $employees = new Employee;
            $employees->firstname = $request->input('firstname');
            $employees->familyname = $request->input('familyname');
            $employees->contactnumber = $request->input('contactnumber');
            $employees->position = $request->input('position');
            $employees->save();
            return redirect('/employeemanagement')->with('info', 'New Employee Added!');

    }

    public function updateemployee($id){
        $employees = Employee::find($id);
        return view('updateemployee', ['employees' => $employees]);
    }


    public function editemployee(Request $request, $id){
        $this->validate($request, [
                'firstname' => 'required',
                'familyname' => 'required',
                'contactnumber' => 'required',
                'position' => 'required'  
            ]);
            $data = array(
                'firstname' => $request->input('firstname'),
                'familyname' => $request->input('familyname'),
                'contactnumber' => $request->input('contactnumber'),
                'position' => $request->input('position')
            );
            Employee::where('id', $id)
            ->update($data);
            return redirect('/employeemanagement')->with('info', 'Employee Profile Updated!');

    }

    public function deleteemployee($id){
        Employee::where('id', $id)
        ->delete();
        return redirect('/employeemanagement')->with('info', 'Employee Successfully Deleted!');
    }


}


/*public function read($id){
        $articles = Article::all();
        return view('read', ['articles' => $articles]);

    }*/