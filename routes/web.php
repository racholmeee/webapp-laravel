<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
});*/

Route::get('/', 'CreatesController@home');
Route::get('/servicemanagement', 'CreatesController@servicemanagement');
Route::get('/menumanagement', 'CreatesController@menumanagement');
Route::get('/employeemanagement', 'CreatesController@employeemanagement'); // employee table //
Route::get('/create', function(){
	return view('create');
});


Route::post('/insert', 'CreatesController@add');
Route::get('/update/{id}', 'CreatesController@update');
Route::post('/edit/{id}', 'CreatesController@edit');
Route::get('/delete/{id}', 'CreatesController@delete');

Route::get('/createmenu', function(){
	return view('createmenu');
});

Route::post('/insertmenu', 'CreatesController@addmenu');
Route::get('/updatemenu/{id}', 'CreatesController@updatemenu');
Route::post('/editmenu/{id}', 'CreatesController@editmenu');
Route::get('/deletemenu/{id}', 'CreatesController@deletemenu');

Route::get('/createemployee', function(){
	return view('createemployee');
});

Route::post('/insertemployee', 'CreatesController@addemployee');
Route::get('/updateemployee/{id}', 'CreatesController@updateemployee');
Route::post('/editemployee/{id}', 'CreatesController@editemployee');
Route::get('/deleteemployee/{id}', 'CreatesController@deleteemployee');







//Route::get('/read/{id}', 'CreatesController@read');
//Route::view('/', "welcome");
//Route::view('/register', "register");