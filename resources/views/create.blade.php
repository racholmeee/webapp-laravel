@include('inc.header')
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<form class="form-horizontal" method="POST" action="{{ url('/insert') }}"> 
					{{csrf_field()}}
					  <fieldset>
					    <legend>Service Management</legend>	
					    @if(count($errors) > 0)
					    	@foreach($errors->all() as $error)
					    		<div class="alert alert-danger">
					    			{{$error}}
					    		</div>
					    	@endforeach
					    @endif
					    <div class="form-group">
					      <label for="exampleInputEmail1">Package Name</label>
					      <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Enter Package Name">
					      <small id="emailHelp" class="form-text text-muted">Name of type of package from the catering services.</small>
					    </div>
					      <label for="exampleTextarea">Package Description</label>
					      <textarea class="form-control" name="description" id="exampleTextarea" rows="3" placeholer="Package Description"></textarea>
					    <br>
					    
					    <div class="form-group">
					    	<div class="col-lg-10 col-lg-offset-2">
					    		<button type="submit" class="btn btn-primary">Submit</button>
					      
					      <a href="{{ url('/servicemanagement') }}" class="btn btn-secondary">Back </a>
					  </div>
					</div>
					</div>	
					      </fieldset>

				</form>
			</div>
		</div>
	</div>

