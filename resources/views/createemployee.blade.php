@include('inc.header')
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<form class="form-horizontal" method="POST" action="{{ url('/insertemployee') }}"> 
					{{csrf_field()}}
					  <fieldset>
					    <legend>Employee Management</legend>	
					    @if(count($errors) > 0)
					    	@foreach($errors->all() as $error)
					    		<div class="alert alert-danger">
					    			{{$error}}
					    		</div>
					    	@endforeach
					    @endif
					    <div class="form-group">
					      <label for="exampleInputEmail1">First Name</label>
					      <input type="text" class="form-control" name="firstname" id="exampleInputEmail1" placeholder="Enter Employee's First Name">
					      <small id="emailHelp" class="form-text text-muted">First Name of the Employee.</small>
					    </div>
					    <div class="form-group">
					      <label for="exampleInputEmail1">Last Name</label>
					      <input type="text" class="form-control" name="familyname" id="exampleInputEmail1" placeholder="Enter Employee's Last Name">
					      <small id="emailHelp" class="form-text text-muted">Last Name of the Employee.</small>
					    </div>
					      <label for="exampleTextarea">Contact Number</label>
					      <input type="text" class="form-control" name="contactnumber" id="exampleInputEmail1" placeholder="Enter Contact Number">
					    <br>
					    <label for="exampleTextarea">Job Position</label>
					    <input type="text" class="form-control" name="position" id="exampleInputEmail1" placeholder="Enter Job Position">
					    <br>
					    
					    <div class="form-group">
					    	<div class="col-lg-10 col-lg-offset-2">
					    		<button type="submit" class="btn btn-primary">Submit</button>
					      
					      <a href="{{ url('/employeemanagement') }}" class="btn btn-secondary">Back </a>
					  </div>
					</div>
					</div>	
					      </fieldset>

				</form>
			</div>
		</div>
	</div>

