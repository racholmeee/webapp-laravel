@include('inc.header')
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<form class="form-horizontal" method="POST" action="{{ url('/insertmenu') }}"> 
					{{csrf_field()}}
					  <fieldset>
					    <legend>Menu Management</legend>	
					    @if(count($errors) > 0)
					    	@foreach($errors->all() as $error)
					    		<div class="alert alert-danger">
					    			{{$error}}
					    		</div>
					    	@endforeach
					    @endif
					    <div class="form-group">
					      <label for="exampleInputEmail1">Menu Name</label>
					      <input type="text" class="form-control" name="food" id="exampleInputEmail1" placeholder="Enter Name of Dish">
					      <small id="emailHelp" class="form-text text-muted">Name of the Menu/Dish.</small>
					    </div>
					      <label for="exampleTextarea">Price of the Menu</label>
					      <input type="number" class="form-control" name="price" id="exampleInputEmail1" placeholder="Enter Price">
					    <br>
					    
					    <div class="form-group">
					    	<div class="col-lg-10 col-lg-offset-2">
					    		<button type="submit" class="btn btn-primary">Submit</button>
					      
					      <a href="{{ url('/menumanagement') }}" class="btn btn-secondary">Back </a>
					  </div>
					</div>
					</div>	
					      </fieldset>

				</form>
			</div>
		</div>
	</div>

