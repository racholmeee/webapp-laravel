<!DOCTYPE html>
<html>
<head>
	<title>Angkong's Times Kitchen</title>
	<link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">
	<script type="text/javascript" src="{{ url('js/jquery-3.1.0.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/bootstrap.js') }}"></script>
  <style>
      body{
    background-image: url('../resources/views/inc/foodback2.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    overflow-x: hidden;
    color: black;

  }

  </style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="{{ url('/') }}">Angkong's Times Kitchen</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/servicemanagement') }}">Service Management <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/menumanagement') }}">Menu Management</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/employeemanagement') }}">Employee Management</a>
      </li>
    </ul>
  </div>
</nav>